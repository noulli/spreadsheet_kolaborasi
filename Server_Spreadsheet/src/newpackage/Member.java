/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package newpackage;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author Joice
 */
public class Member {
    int id;
    String username;
    String password;
    String nama;
    String alamat;
    String gender;
    
    private Connection con;
    private Statement stat;
    private ResultSet rs;
    
    public Connection getConnection()
    {
        setCon(null);
        try{
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            setCon((Connection) DriverManager.getConnection("jdbc:mysql://localhost/uas_spreadsheet","root","mysql"));
        }
        catch(Exception ex)
        {
            
        }
        return getCon();
    }
    
    public Member()
    {
        getConnection();
    }
    
    public Member(int thisId, String thisUsername, String thisPassword, String thisNama,
                  String thisAlamat, String thisGender)
    {
        this.username = thisUsername;
        this.password = thisPassword;
        this.nama = thisNama;
        this.alamat = thisAlamat;
        this.gender = thisGender;
        getConnection();
    }
    
    public String insertMember(String thisUsername, String thisPassword, String thisNama,
                             String thisAlamat, String thisGender)
    {
        String hasil = "";
        try
        {
            stat = (Statement)con.createStatement();
            if(!con.isClosed())
            {
                String query = "INSERT INTO member(id_member, username, password, nama, alamat, gender)"
                             + "values(" + 0 + ", '" + thisUsername + "', '"+ thisPassword + "', '"+ thisNama + "', '"
                             + thisAlamat + "', '"+ thisGender.toUpperCase() + "')";
                stat.executeUpdate(query);
                hasil = "SUKSES";
            }
        }
        catch (Exception e)
        {
            System.err.println("ExceptionInsertMember: " + e.getMessage());
            hasil = "GAGAL";
        }
        return hasil;
    }
    
    public String ubahMember(String thisUsername, String thisPassword, String thisNama,
                             String thisAlamat, String thisGender)
    {
        String hasil = "";
        try
        {
            stat = (Statement)con.createStatement();
            if(!con.isClosed())
            {
                String query = "UPDATE member SET password = '" + thisPassword + "', nama = '" + thisNama + "',"
                               + "alamat = '" + thisAlamat + "', gender = '" + thisGender.toUpperCase() + "'"
                               + "WHERE username = '" + thisUsername + "' ";
                stat.executeUpdate(query);
                hasil = "SUKSES";
            }
        }
        catch (Exception e)
        {
            System.err.println("ExceptionUbahProfil: " + e.getMessage());
            hasil = "GAGAL";
        }
        return hasil;
    }
    
    ArrayList<Member> getMember()
    {
        ArrayList<Member> myUser = new ArrayList<>();
        try
        {
            setStat((Statement) getCon().createStatement());
            setRs(getStat().executeQuery("SELECT * FROM member"));
            while(getRs().next())
            {
                Member user = new Member(getRs().getInt("id_member"), getRs().getString("username"),
                              getRs().getString("password"), getRs().getString("nama"),
                              getRs().getString("alamat"),getRs().getString("gender"));
                myUser.add(user);
            }
        }
        catch(Exception ex)
        {
            
        }
        return myUser;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the nama
     */
    public String getNama() {
        return nama;
    }

    /**
     * @param nama the nama to set
     */
    public void setNama(String nama) {
        this.nama = nama;
    }

    /**
     * @return the alamat
     */
    public String getAlamat() {
        return alamat;
    }

    /**
     * @param alamat the alamat to set
     */
    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    /**
     * @return the gender
     */
    public String getGender() {
        return gender;
    }

    /**
     * @param gender the gender to set
     */
    public void setGender(String gender) {
        this.gender = gender;
    }
    
    /**
     * @return the con
     */
    public Connection getCon() {
        return con;
    }

    /**
     * @param con the con to set
     */
    public void setCon(Connection con) {
        this.con = con;
    }

    /**
     * @return the stat
     */
    public Statement getStat() {
        return stat;
    }

    /**
     * @param stat the stat to set
     */
    public void setStat(Statement stat) {
        this.stat = stat;
    }

    /**
     * @return the rs
     */
    public ResultSet getRs() {
        return rs;
    }

    /**
     * @param rs the rs to set
     */
    public void setRs(ResultSet rs) {
        this.rs = rs;
    }
}
