/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package newpackage;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;


public class ChatServer {

    public static ArrayList<ChatUser> DaftarUser = new ArrayList<>();
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        ServerSocket ss;
        int port = 8888;
        try {
            // Buka ServerSocket
            ss = new ServerSocket(port);
            System.out.println("Server dimulai.");
            while(true) {
                System.out.println("Menunggu user...");
                Socket incoming = ss.accept();
                System.out.println("User baru terhubung dari " 
                        + incoming.getInetAddress());
                ChatUser user = new ChatUser(incoming);
                DaftarUser.add(user);
                user.start();
            }
        } catch (IOException ex) {
            Logger.getLogger(ChatServer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
