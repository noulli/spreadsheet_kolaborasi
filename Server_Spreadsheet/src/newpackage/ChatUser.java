/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package newpackage;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;


public class ChatUser extends Thread {
    Socket socket;
    BufferedReader input;
    PrintStream output;
    String usern, passw, nama, alamat, gender;
    String chatting;
    
    Member temp = new Member();
    ArrayList<Member> user = temp.getMember();
    
    Sheet tempSheet = new Sheet();
    ArrayList<Sheet> lembar = tempSheet.getSheet();

    public String getNama() {
        return usern;
    }

    public void setUsern(String usern) {
        this.usern = usern;
    }

    public ChatUser(Socket socket) throws IOException {
        this.socket = socket;
        input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        output = new PrintStream(socket.getOutputStream());
    }

    @Override
    public void run() {
        while(true) {
            try {
                Thread.sleep(10);
                // apakah kita membutuhkan thread.sleep? dan why?
                
                String message = input.readLine();
                if(message == null)
                {
                    System.out.println(usern + " mengirim NULL");
                    break;
                }
                else if(message != null)
                {
                    if(message.equalsIgnoreCase("/q"))
                    {
                        broadcast(usern + " OFFLINE", this);
                        output.println("/q");
                        break;
                    }
                    else if(message.startsWith("/login"))
                    {
                        // Format pesan: /login;username;password
                        usern = message.split(";")[1];
                        passw = message.split(";")[2];
                        if(cekLogin(usern, passw).equals("SUKSES"))
                        {
                            
                            broadcast(usern + " ONLINE", this);
                            output.println("/masuk;" + usern + "; ONLINE");
                        }
                        else
                        {
                            output.println("/salah");
                            break;
                        }
                    }
                    else if(message.startsWith("/daftar"))
                    {
                        usern = message.split(";")[1];
                        passw = message.split(";")[2];
                        nama = message.split(";")[3];
                        alamat = message.split(";")[4];
                        gender = message.split(";")[5];
                        if(getMember().contains(usern.toLowerCase()) == true ||
                           getMember().contains(usern.toUpperCase()) == true)
                        {
                            output.println("/duplikat");
                        }
                        else
                        {
                            if(InsertMember(usern, passw, nama, alamat, gender).equals("SUKSES"))
                            {
                                output.println("/daftar");
                            }
                            else
                            {
                                output.println("/xdaftar");
                                break;
                            }
                        }
                    }
                    else if(message.startsWith("/profil"))
                    {
                        usern = message.split(";")[1];
                        passw = message.split(";")[2];
                        nama = message.split(";")[3];
                        alamat = message.split(";")[4];
                        gender = message.split(";")[5];
                        if(passw.equals(""))
                        {
                            output.println("/passprofil");
                        }
                        else if(UbahProfil(usern, passw, nama, alamat, gender).equals("SUKSES"))
                        {
                            output.println("/profil");
                        }
                    }
                    else if(message.startsWith("/tampilsheet"))
                    {
                        //broadcast(getIsiSheet().size() + " SIZE", this);
                        for(int i = 0; i < getIsiSheet().size(); i++)
                        {
                            output.println("/tampilsheet" + ";" + getIsiSheet().get(i));
                        }
                    }
                    else if(message.startsWith("/ubahsheet"))
                    {
                        Sheet tempSheet2 = new Sheet();
                        ArrayList<Sheet> lembar2 = tempSheet.getSheet();
                        
                        ArrayList<String>isiSheet = new ArrayList<String>();
                        for(int i=0;i<lembar2.size();i++)
                        {
                            isiSheet.add(lembar2.get(i).baris + ";" + lembar2.get(i).kolom);
                        }
                        
                        String baris = message.split(";")[1];
                        String kolom = message.split(";")[2];
                        String isi = message.split(";")[3];
                        if(isiSheet.toString().contains(baris + ";" + kolom))
                        {
                            UbahSheet(baris,kolom,isi);
                            broadcast(baris + ";" + kolom + ";" + isi + ";" + "SHEET", this);
                            output.println("/ubahsheet");
                        }
                        else
                        {
                            InsertSheet(baris,kolom,isi);
                            broadcast(baris + ";" + kolom + ";" + isi + ";" + "SHEET", this);
                            output.println("/ubahsheet");
                        }
                    }
                    else
                    {
                        broadcast(this.usern + ": " + message, this);
                    }
                }
            } catch (InterruptedException ex) {
                Logger.getLogger(ChatUser.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(ChatUser.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public void kirimPesan(String pesan) {
        output.println(pesan);
    }

    public static void broadcast(String pesan, ChatUser kecuali) {
        for (ChatUser user : ChatServer.DaftarUser) {
            if(kecuali == null) {
                user.kirimPesan(pesan);
            }
            else if(user != kecuali) {
                user.kirimPesan(pesan);
            }
            else{}
        }
    }
    
    public static void broadcast(String pesan) {
        broadcast(pesan);
    }
    
    public ChatUser cariUserDenganNama(String nama) {
        for (ChatUser user : ChatServer.DaftarUser) 
            if(user.getNama().equals(nama)) 
                return user;
        return null;
    }
    
    public String cekLogin(String username, String password) {
        String hasil = "";
        for(int i=0;i<user.size();i++)
        {
            if(username.equals(user.get(i).username) && password.equals(user.get(i).password))
            {hasil = "SUKSES";}
        }
        return hasil;
    }
    public ArrayList<String> getMember() {
        ArrayList<String>member = new ArrayList<String>();
        for(int i=0;i<user.size();i++)
        {
            member.add(user.get(i).username);
        }
        return member;
    }
    public String InsertMember(String username, String password, String nama, String alamat,
                               String gender) {
        String masuk = temp.insertMember(username, password, nama, alamat, gender);
        return masuk;
    }
    public String UbahProfil(String username, String password, String nama, String alamat,
                               String gender) {
        String masuk = temp.ubahMember(username, password, nama, alamat, gender);
        return masuk;
    }
    
    public String InsertSheet(String baris, String kolom, String isi) {
        String masuk = tempSheet.insertSheet(baris, kolom, isi);
        return masuk;
    }
    public String UbahSheet(String baris, String kolom, String isi) {
        String masuk = tempSheet.ubahSheet(baris, kolom, isi);
        return masuk;
    }
    public ArrayList<String> getIsiSheet() {
        ArrayList<String>isiSheet = new ArrayList<String>();
        for(int i=0;i<lembar.size();i++)
        {
            isiSheet.add(lembar.get(i).baris + ";" + lembar.get(i).kolom + ";" + lembar.get(i).isi);
        }
        return isiSheet;
    }
    /*public ArrayList<String> getBarisKolomSheet() {
        ArrayList<String>isiSheet = new ArrayList<String>();
        for(int i=0;i<lembar.size();i++)
        {
            isiSheet.add(lembar.get(i).baris + ";" + lembar.get(i).kolom);
        }
        return isiSheet;
    }*/
}
