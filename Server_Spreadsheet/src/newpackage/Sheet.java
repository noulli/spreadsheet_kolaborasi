/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package newpackage;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author Joice
 */
public class Sheet {
    int id;
    String baris;
    String kolom;
    String isi;
    
    private Connection con;
    private Statement stat;
    private ResultSet rs;
    
    public Connection getConnection()
    {
        setCon(null);
        try{
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            setCon((Connection) DriverManager.getConnection("jdbc:mysql://localhost/uas_spreadsheet","root","mysql"));
        }
        catch(Exception ex)
        {
            
        }
        return getCon();
    }
    
    public Sheet()
    {
        getConnection();
    }
    
    public Sheet(int thisId, String thisBaris, String thisKolom, String thisIsi)
    {
        this.baris = thisBaris;
        this.kolom = thisKolom;
        this.isi = thisIsi;
        getConnection();
    }
    
    public String insertSheet(String thisBaris, String thisKolom, String thisIsi)
    {
        String hasil = "";
        try
        {
            stat = (Statement)con.createStatement();
            if(!con.isClosed())
            {
                String query = "INSERT INTO sheet(id_sheet, baris, kolom, isi)"
                             + "values(" + 0 + ", '" + thisBaris + "', '"+ thisKolom + "', '"+ thisIsi + "')";
                stat.executeUpdate(query);
                hasil = "SUKSES";
            }
        }
        catch (Exception e)
        {
            System.err.println("ExceptionInsertSheet: " + e.getMessage());
            hasil = "GAGAL";
        }
        return hasil;
    }
    
    public String ubahSheet(String thisBaris, String thisKolom, String thisIsi)
    {
        String hasil = "";
        try
        {
            stat = (Statement)con.createStatement();
            if(!con.isClosed())
            {
                String query = "UPDATE sheet SET isi = '" + thisIsi + "'"
                        + "WHERE baris = '" + thisBaris + "' && kolom = '" + thisKolom + "'";
                stat.executeUpdate(query);
                hasil = "SUKSES";
            }
        }
        catch (Exception e)
        {
            System.err.println("ExceptionUbahSheet: " + e.getMessage());
            hasil = "GAGAL";
        }
        return hasil;
    }
    
    ArrayList<Sheet> getSheet()
    {
        ArrayList<Sheet> mySheet = new ArrayList<>();
        try
        {
            setStat((Statement) getCon().createStatement());
            setRs(getStat().executeQuery("SELECT * FROM sheet"));
            while(getRs().next())
            {
                Sheet lembar = new Sheet(getRs().getInt("id_sheet"), getRs().getString("baris"),
                              getRs().getString("kolom"), getRs().getString("isi"));
                mySheet.add(lembar);
            }
        }
        catch(Exception ex)
        {
            
        }
        return mySheet;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the baris
     */
    public String getBaris() {
        return baris;
    }

    /**
     * @param baris the baris to set
     */
    public void setBaris(String baris) {
        this.baris = baris;
    }

    /**
     * @return the kolom
     */
    public String getKolom() {
        return kolom;
    }

    /**
     * @param kolom the kolom to set
     */
    public void setKolom(String kolom) {
        this.kolom = kolom;
    }

    /**
     * @return the isi
     */
    public String getIsi() {
        return isi;
    }

    /**
     * @param isi the isi to set
     */
    public void setIsi(String isi) {
        this.isi = isi;
    }

    /**
     * @return the con
     */
    public Connection getCon() {
        return con;
    }

    /**
     * @param con the con to set
     */
    public void setCon(Connection con) {
        this.con = con;
    }

    /**
     * @return the stat
     */
    public Statement getStat() {
        return stat;
    }

    /**
     * @param stat the stat to set
     */
    public void setStat(Statement stat) {
        this.stat = stat;
    }

    /**
     * @return the rs
     */
    public ResultSet getRs() {
        return rs;
    }

    /**
     * @param rs the rs to set
     */
    public void setRs(ResultSet rs) {
        this.rs = rs;
    }
}
